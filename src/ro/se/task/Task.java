package ro.se.task;

public class Task {
	
	private String name;
	public static int taskCounter=0;
	private Runnable runnable;
	
	public Task(){
		name = generateName();
	}

	public Task(String name){
	//	this(name);
		this.name = name;
	}
	
	public Task(String name, Runnable runnable){
		this.runnable = runnable;
	}
	
	public void run(){
		runnable.run();
	}
	
	
	
	private String generateName() {
		// TODO Auto-generated method stub
		return null;
	}
}
