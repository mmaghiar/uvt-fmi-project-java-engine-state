package ro.se.transition;

public enum ConnectionTransition {
	
	START(STOPPED, PENDING_START),
	START_CONFIRMED(PENDING_START, STARTED),
	START_FAILED(PENDING_START, STOPPED),
	STOP(STARTED, PENDING_STOP), PENDING_START;
	
	private ConnectionStatus first, second;
	
	private ConnectionTransition(ConnectionStatus first, ConnectionStatus second){
		this.first = first;
		this.second = second;
	}
	
}
