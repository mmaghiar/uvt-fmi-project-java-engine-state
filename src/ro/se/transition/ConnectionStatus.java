package ro.se.transition;

import ro.se.state.IState;

public enum ConnectionStatus implements IState{
	STARTED,
	STOPPED,
	PENDING_START,
	PENDING_STOP,
	DISPOSED;
}
